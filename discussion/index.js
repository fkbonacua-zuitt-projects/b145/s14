// displays the message
console.log("Hello from JS");

// this is a single-line comment

/*multi-line comment
console.log("Hello from JS");
console.log("Hello from JS");
console.log("Hello from JS");
console.log("Hello from JS");
console.log("Hello from JS");
*/

// ctrl+ /
// console.log("end of comment");

console.log("example statement");

// syntax: console.log(value/message);

// [DECLARING VARIABLES]
// -> tell our devices that a variable name is created and ready to store data

//  syntax: let/const desiredVariableName;

let myVariable;

// WHAT is a variable?
// ==>It is used to contain/store data

// Benefits of utilizing a variable?
	// This makes it easier for us to associate information stored in our devices to actual names about the information.

// an assignment operator (=) is used to assign/pass down values into a variable
let clientName = "Juan Dela Cruz";
let contactNo ="09951446335";

// [PEEKING INSIDE A VARIABLE]
let greetings;
let pangalan = "John Doe";

console.log(clientName);
console.log(contactNo);
console.log(greetings);
console.log(pangalan);

// if we would try to print out a value of a variable that has not been declared a value, it will return an error of "undefined".

// [DATA TYPES]

// 3 Forms of Data Types:

// 1. Primitive -> contains only a single value:
// 		ex: strings, numbers, boolean
// 2. Composite -> can contain multiple values.
// 		ex: array, objects
// 3. Special:
// 		ex: null, undefined

// 1.String
let country = "Philippines";
let province = 'Metro manila';
// 2.Number
let headcount = 26;
let grade = 98.7;
// 3.Boolean
let isMarried = false;
let inGoodConduct = true;
// 4.Null
let spouse = null;
let criminalRecords = true;
// 5.Arrays
	// Arrays are a special kind of composite data type that is used to store multiple values.

	// lets create a collection of all your subjects in the bootcamp
let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javascript"];

// displat the output of the array inside the console
console.log(bootcampSubjects);

// Rule of Thumb when declaring array structures
// ==> Storing multiple data types inside an array is NOT recommended. in a context of programming this does not make any sense.
// an array should be a collection of data that describes a similar/single topic or subject.

let details = ["Keanu", "Reeves", 32, true];
console.log(details);

// Ojects
	// Objects are another special kind of composite data type that is used to mimic or represent a real world object/item.
	// They are used to create complex data that contains pieces of information that are relevant to each other.
	// Every individual piece of code/information is called property of an object.

	// SYNTAX:
		// let/const objectName = {
		// 	key -> value
		// 	propertyA: value,
		// 	propertyB: value
		// }

	// lets create an object that describes the properties of a cellphone

let cellphone = {
		brand: 'Samsung' ,
		model: 'A12',
		color: 'Black',
		serialNo: 'AX12002122',
		isHomeCredit: true,
		features: ["Calling", "Texting", "Ringing", "5G"],
		price: 8000
}

// lets display the object inside the console.
console.log(cellphone);

// Variables and Constants

// Variables -> are used to store data.

// the values/info stored inside a variable can be changed or repackaged.
let personName = "Michael";
console.log(personName);

// if youre going to reassign a new value for a variable, you no longer have to use the "let" again.
personName = "Jordan";
console.log(personName);

// cancatenating string (+)
	// => join, combine, link
let pet = "dog";
console.log("this is the initial value of variable: " + pet);

pet = "cat";
console.log("This is the new value of the var: " + pet);

// Constants
	// =>Permanent, fixed, absolute
// => the value assigned on a constant cannot be changed.
// syntax: const desiredName = value;
const PI = 3.14;
console.log(PI);

const year = 12;
console.log(year);

const familyName = 'Dela Cruz';
console.log(familyName);

// lets try to reassign a new value in a constant.
// familyName = 'Castillo';
// console.log(familyName);

